package de.oszimt.fos42.homecontrolfx.api;

public class TestHomeControlSim {

	public static void main(String[] args) {
		// New Home Control Server
		HomeControlSim mySimulatedServer = new HomeControlSim();

		// Generate the auth key
		String myKey = mySimulatedServer.generateAuthKey("Hallo", "Welt!");
		System.out.println("Received token: " + myKey);
		mySimulatedServer.setAuthKey(myKey);
		if (!mySimulatedServer.checkAuthKey())
		{
			System.out.println("Login error - Token:" + myKey);
			return;
		}
		System.out.println("Login ok - User: " + mySimulatedServer.getUserName());
		// Get temp
		double curTemp = mySimulatedServer.getTemperature();
		System.out.println("Temperature is " + curTemp + "°C");			
		// Get relais state
		boolean curState = mySimulatedServer.getLightState();
		System.out.println("Relais is " + (curState ? "on" : "off") + "!");
		System.out.println("Relais changed to " + (mySimulatedServer.switchLightState() ? "on" : "off") + "!");
		System.out.println("Relais is " + (curState ? "on" : "off") + "!");
		System.out.println("Relais changed to " + (mySimulatedServer.switchLightState() ? "on" : "off") + "!");
		// Get temp
		curTemp = mySimulatedServer.getTemperature();
		System.out.println("Temperature is " + curTemp + "°C");			
	}

}
